<?php
require_once("Trello2MWClass.php");

function yescheck($value){
    if($value){
        return '{{YesCheck}}';
    }
    return '';
}

function datetimeformat($stringdate){
    if(empty($stringdate)){
        return "";
    }
    $dt = new DateTime($stringdate);
    return $dt->format("H:i j.n.Y");
}

function linkify($link){
    return "<a href='" . $link . "'>Přejít na úkol</a>";
}

function listname($list){
    return Trello2MWBackend::get_list_name($list);
}

function process_members($members, $taskmembers){
    $strings = Array();
    foreach($taskmembers as $tm){
        if(!isset($members[$tm])){
            $members = Trello2MWBackend::reload_members();
        }
            $strings[] = $members[$tm]["fullName"];

    }
    natcasesort($strings);
    return implode(", ", $strings);
}


class Trello2MW extends SpecialPage{
    function __construct(){
        parent::__construct("Úkoly", '', true, false, 'default', True);
    }

    function execute($par){
        global $wgOut;
        global $wgUser;
        $output = &$wgOut;

        //IAC speciality
        $allowed_groups = "Team";
        if(! efIACUserCanAccess($wgUser, $allowed_groups, "read")){
            die("Login please!");
        }

        if(!empty($par) and Trello2MWBackend::is_user($par)){
            $output->addHTML("<h2>" . $par . " (Úkoly)</h2>");
        }
        else {
            $output->addHTML("<h2>Všechny úkoly</h2>");
        }

        $table = "<table class='wikitable'>\n<tr>
        <th>Úkol</th>
        <th>Hotovo</th>
        <th>Deadline</th>
        <th>Kdo</th>
        <th>Poznámka</th>
        <th>Seznam</th>
        <th>Odkaz</th>
        </tr>";
        $members = Trello2MWBackend::load_members();

        foreach(Trello2MWBackend::get_user_tasks($par) as $task){
            $task = (array) $task;
            //var_dump($task);
            $table .= "<tr>";
            $table .= "<td><b>". $task["name"] . "</b></td>";
            $table .= "<td>". yescheck($task["closed"]) . "</td>";
            $table .= "<td>". datetimeformat($task["due"]) . "</td>";
            $table .= "<td>". process_members($members, $task["idMembers"]) . "</td>";
            $table .= "<td>". $task["desc"] . "</td>";
            $table .= "<td>". listname($task["idList"]) . "</td>";
            $table .= "<td>". linkify($task["shortUrl"]) . "</td>";
            $table .= "</tr>";
        }
        $table .= "</table>";
        $output->addHTML($table);
        //exit;
     }
}