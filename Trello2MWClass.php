<?php
require_once("curl.class.php");
require_once("dibi.min.php");

define("KEY","321c6a8928f7229143940ba6361d8f06");
define("SECRET","3d3f1cd98773a0a5baa9e025f741f85f9d779167836713a5958ffa5747e8c1cc");
define("TOKEN","9cc13ad04c1dc5cb717ba16c3380dd3448f7c50041d3cf67653357e81183c00f");
define("ALL","absopositivelyall");
define("MEMBERS","veryallmembers");
define("BOARD","528e407d70b9a7bf2d008466");

dibi::connect(Array(
    "driver" => "sqlite3",
    "file" => "trello2mw/trello2mw.db",
    "formatDate" => "j.n.Y",
    "formatDateTime" => "H:m j.n.Y"
));



class CacheTooOld extends Exception {
    public function __construct(){}
}
class ConnectionFailed extends Exception { }

Class Trello {
    private static function query_trello($url){
        $curl = new Curl();
        $curl->get("https://api.trello.com/1/".$url, Array(
            "key" => KEY,
            "token" => TOKEN
        ));

        if($curl->error_code != 0){
            var_dump($curl->request_headers);
            var_dump($curl->response_headers);
            throw new ConnectionFailed();
        }
        return json_decode($curl->response, True);
    }

    public static function get_user_tasks($username){
        return self::query_trello("boards/".BOARD."/members/".$username."/cards");
    }

    public static function get_all_tasks(){
        return self::query_trello("boards/".BOARD."/cards");
    }

    public static function get_listname($listid){
        return self::query_trello("lists/".$listid."/name");
    }

    public static function load_members(){
        $members = self::query_trello("boards/".BOARD."/members");
        $assoc = Array();
        foreach($members as $member){
            $assoc[$member["id"]] = $member;
        }
        return $assoc;
    }
}

class Cache {
    //tasks cache time
    protected static $tct = 300;
    //listname cache time
    protected static $lct = 43200;
    //member cache time
    protected static $mct = 86400;

    private static function probability_cleanup($table, $cache_time){
        if (rand(1,1000) == 1){
            dibi::query("DELETE FROM %n WHERE [timestamp]<%i", $table, time()-$cache_time);
        }
        return True;
    }

    public static function get_tasks($username){
        $results = dibi::query("SELECT [data] FROM [cache] WHERE [timestamp]>%i AND [username]=%s ORDER BY [timestamp] DESC LIMIT 1", time()-self::$tct, $username)->fetch();
        if(!$results){
            throw new CacheTooOld();
        }
        return unserialize($results["data"]);
    }

    public static function cache_tasks($username, $tasks){
        self::probability_cleanup("tasks", self::$tct);
        return dibi::query("INSERT INTO [cache] ([username],[data],[timestamp]) VALUES (%s,%s,%i)", $username, serialize($tasks), time());
    }

    public static function get_listname($listid){
        $listname = dibi::query("SELECT [name] FROM [listname] WHERE [listid]=%s AND [timestamp]>%i ORDER BY [timestamp] LIMIT 1", $listid, time()-self::$lct)->fetchSingle();
        if(empty($listname)){
            throw new CacheTooOld();
        }
        return $listname;
    }

    public static function cache_listname($listid, $listname){
        self::probability_cleanup("listname", self::$lct);
        dibi::query("INSERT INTO [listname] ([listid],[name],[timestamp]) VALUES (%s,%s,%i)", $listid, $listname, time());
    }

    public static function cache_members($members){
        return dibi::query("INSERT INTO [cache] ([username],[data],[timestamp]) VALUES (%s,%s,%i)", MEMBERS, serialize($members), time());
    }

    public static function get_members(){
        $results = dibi::query("SELECT [data] FROM [cache] WHERE [timestamp]>%i AND [username]=%s ORDER BY [timestamp] DESC LIMIT 1", time()-self::$mct, MEMBERS)->fetch();
        if(!$results){
            throw new CacheTooOld();
        }
        return unserialize($results["data"]);
    }
}




class Trello2MWBackend {
    private static function get_trello_name($mwname){
        return dibi::query("SELECT [trello] FROM [usermap] WHERE [mediawiki]=%s",$mwname)->fetchSingle();
    }

    protected static function get_tasks(){
        try{
            $tasks = Cache::get_tasks(ALL);
        }
        catch (CacheTooOld $e){
            $tasks = Trello::get_all_tasks();
            Cache::cache_tasks(ALL, $tasks);
        }
        return $tasks;
    }

    public static function is_user($username){
        $user = self::get_trello_name($username);
        if(!empty($user)){
            return True;
        }
        return False;
    }


    public static function get_user_tasks($mwname){
        $username = self::get_trello_name($mwname);
        if(empty($username)) return self::get_tasks();

        try{
            $tasks = Cache::get_tasks($username);
        }
        catch (CacheTooOld $e){
            $tasks = Trello::get_user_tasks($username);
            Cache::cache_tasks($username, $tasks);
        }
        return $tasks;
    }

    public static function get_list_name($listid){
        try {
            $listname = Cache::get_listname($listid);
        }
        catch (CacheTooOld $e){
            $listname = Trello::get_listname($listid);
            Cache::cache_listname($listid, $listname);
        }
        return $listname;
    }

    public static function load_members(){
        try {
            $members = Cache::get_members();
        }
        catch (CacheTooOld $e){
            $members = Trello::load_members();
            Cache::cache_members($members);
        }
        return $members;
    }

    public static function reload_members(){
        $members = Trello::load_members();
        Cache::cache_members($members);
        return $members;
    }
}



























