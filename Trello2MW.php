<?php
global $IP;
if ( !defined( 'MEDIAWIKI' ) ) {
    exit( 1 );
}

$wgExtensionCredits[ 'specialpage' ][] = array(
    'path' => __FILE__,
    'name' => 'Trello2MW',
    'version' => '0.0.1',
);

$wgAutoloadClasses[ 'Trello2MW' ] = "$IP/trello2mw/Trello2MWSpecialPage.php"; # Location of the SpecialMyExtension class (Tell MediaWiki to load this file)
$wgSpecialPages[ 'Úkoly' ] = 'Trello2MW'; # Tell MediaWiki about the new special page and its class name